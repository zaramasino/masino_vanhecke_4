/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package masino_vanhecke_project4;


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author brijolievanhecke
 */
public class Masino_VanHecke_Project4 extends javax.swing.JFrame {

    private HashMap<Client, FamilyTree> clients;

    private HashMap<String, FamilyMember> test;

    private List<String> clientNames;

    private Client currentClient;

    private FamilyTree currentTree;

    private JTree tree;

    DefaultTreeModel dTree;

    JFrame frame = this.frame;
    
    JPanel treePanel;
    //JFrame jf;

    public void switchClient(Client client) {

        //currentTree = clients.get(client);
        for (Map.Entry<Client, FamilyTree> entry : clients.entrySet()) {
            Client key = entry.getKey();
            if (clients.containsKey(client)) {
                currentClient = key;
                client.setFamilyTree(currentTree);
                currentTree = currentClient.getFamilyTree();
            }

        }
    }

    public void displayTree(FamilyTree familyTree) {
        System.out.println("entered display method");
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(currentTree.getRoot().getName());     
        createTree(rootNode, currentTree.getRoot());
        dTree = new DefaultTreeModel(rootNode);
        tree = new JTree(dTree);
        JScrollPane scrollP = new JScrollPane(tree);
        treePanel.add(scrollP);
        treePanel.add(tree);


    }

    /**
     * action invoked when the user selects a node from the tree
     */
    private class treeSelectorAction implements TreeSelectionListener {

        public void valueChanged(TreeSelectionEvent event) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

            //no selection
            if (node == null) {
                return;
            }

            //if the selection is a familymember object
            Object nodeInfo = node.getUserObject();
            if (nodeInfo instanceof FamilyMember) {
                //display details
                displayInfo((FamilyMember) nodeInfo);

            }
        }
    }

    /**
     * Recursive method to populate the jtree object for each family member of
     * the root person
     *
     * @param top the node to populate
     * @param root the member to get the details from
     */
    private void createTree(DefaultMutableTreeNode top, FamilyMember root) {
        DefaultMutableTreeNode parents = null;
        DefaultMutableTreeNode parent = null;
        DefaultMutableTreeNode spouseNode = null;
        DefaultMutableTreeNode spouse = null;
        DefaultMutableTreeNode children = null;
        DefaultMutableTreeNode child = null;  
        
        
        HashMap<String, FamilyMember> parentsList = root.getParents();
        int parentSize = parentsList.size();

        if (root.getParents() != null && root == currentTree.getRoot()) {
            parents = new DefaultMutableTreeNode("Parents");
            for (Map.Entry<String, FamilyMember> set
                    : parentsList.entrySet()) {

                FamilyMember value = set.getValue();
                parent = new DefaultMutableTreeNode(value);
                createTree(parent, value);
                System.out.println("added parent " + value.getName());
                parents.add(parent);
            }
            top.add(parents);

            if (root.getSpouse() != null) {
                spouseNode = new DefaultMutableTreeNode("Spouse");
                spouse = new DefaultMutableTreeNode(root.getSpouse());
                //add spouse node
                spouseNode.add(spouse);
                System.out.println("added spouse " + root.getSpouse().getName());
                //add the spouse node
                top.add(spouseNode);
            }

            HashMap<String, FamilyMember> childrenList = root.getChildren();
            int ChildrenSize = childrenList.size();

            if (ChildrenSize > 0) {
                children = new DefaultMutableTreeNode("Children");
                for (Map.Entry<String, FamilyMember> set
                        : childrenList.entrySet()) {

                    FamilyMember value = set.getValue();
                    child = new DefaultMutableTreeNode(value);
                    createTree(child, value);
                    System.out.println("added child " + value.getName());
                    children.add(child);
                }
                top.add(children);
            }
        }

    }

    public void addClient(String name) {

        Client addedclient = new Client(name);

        String clientName = addedclient.getName();

        clientNames.add(clientName);

        FamilyTree ft = new FamilyTree();

        clients.put(addedclient, ft);

    }

    public void displayInfo(FamilyMember member) {

        UIManager.put("OptionPane.minimumSize", new Dimension(500, 500));

        JOptionPane.showMessageDialog(this, "Name: " + member.getName() + "\n DOB: " + member.getDateOfBirth(), "Personal Information of " + member.getName(), JOptionPane.INFORMATION_MESSAGE);

        //Just an example of everything we want to display for one member
        System.out.println(member.getName());

        System.out.println(member.getDateOfBirth());

        System.out.println(member.getStatus());

        System.out.println(member.getChildren());

        System.out.println(member.getParents());

        System.out.println(member.getSpouse());

    }

    public void createGUI() {
        currentTree = new FamilyTree();
        tree = new JTree();
        //jf = new JFrame("JTree");
    }

    public void confirmSavedData() {

    }

    public void confirmLoadData() {

    }

    public void saveAs(HashMap<String, FamilyMember> map, int size) throws FileNotFoundException, IOException, ClassNotFoundException {

        FileOutputStream f = new FileOutputStream(new File("myObjects.txt"));
        ObjectOutputStream o = new ObjectOutputStream(f);

        o.writeObject(map);

        o.close();
        f.close();

        loadOldVersion(map);

    }

    public void readNewFile() throws IOException, FileNotFoundException, ClassNotFoundException {
        //String filePath = "/Users/brijolievanhecke/NetBeansProjects/masino_vanhecke_4/Masino_VanHecke_Project4/src/masino_vanhecke_project4/testFile.txt";
        String filePath = "C:\\Users\\zara\\Documents\\NetBeansProjects\\masino_vanhecke_4a\\Masino_VanHecke_Project4\\src\\masino_vanhecke_project4\\testFile.txt";
        test = new HashMap<String, FamilyMember>();
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        while ((line = reader.readLine()) != null) {
            if (!(line.trim().isEmpty())) {
                if (line.contains("parentof")) {
                    String[] parts = line.split(",");
                    FamilyMember parent = test.get(parts[0].trim());
                    FamilyMember child = test.get(parts[2].trim());
                    child.addParent(parent);
                    parent.addChild(child);
                    //System.out.println("check parent and child lists");
                    //child.printparentlist();
                    //parent.printchildlist();
                } else if (line.contains("marriedto")) {
                    String[] parts = line.split(",");
                    FamilyMember spouse1 = test.get(parts[0].trim());
                    FamilyMember spouse2 = test.get(parts[2].trim());
                    spouse1.addSpouse(spouse2);
                    spouse2.addSpouse(spouse1);
                    //System.out.println("check spouse lists");
                    //spouse1.printspouse();
                    //spouse2.printspouse();

                } else {
                    String[] parts = line.split(",", 3);
                    FamilyMember newMember = new FamilyMember(parts[0].trim(), parts[1].trim(), parts[2].trim());
                    if (parts[2].contains(",")) {
                        newMember.setStatus(true);
                    } else {
                        newMember.setStatus(false);
                    }
                    //System.out.println(newMember.getStatus());
                    //createClientButton.setText(newMember.getStatus());
                    
                    if (newMember.checkIsAlive().equals(true)){
                        System.out.println("residence " + newMember.getStatus());
                    }
                    else {
                        System.out.println("date of death " + newMember.getStatus());
                    }
                    test.put(parts[0].trim(), newMember);
                }
            }

        }
        System.out.println("info loaded");
        Map.Entry<String, FamilyMember> entry = test.entrySet().iterator().next();
        FamilyMember value = entry.getValue();
        currentTree.setRoot(value);
        displayTree(currentTree);
        saveAs(test, test.size());
        reader.close();
    }

    public void loadOldVersion(HashMap<String, FamilyMember> map) throws FileNotFoundException, IOException, ClassNotFoundException {

        FileInputStream fi = new FileInputStream(new File("myObjects.txt"));
        ObjectInputStream oi = new ObjectInputStream(fi);

        map = (HashMap) oi.readObject();

        oi.close();
        fi.close();

    }

    /**
     * Creates new form project4
     */
    public Masino_VanHecke_Project4() throws IOException, FileNotFoundException, ClassNotFoundException {
        initComponents();
        setSize(getMaximumSize());
        readNewFile();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
// <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        createGUI();

        jPanel1 = new javax.swing.JPanel();
        treePanel = new javax.swing.JPanel();
        switchClientButton = new javax.swing.JButton();
        exitButton = new javax.swing.JButton();
        saveAsButton2 = new javax.swing.JButton();
        createClientButton = new javax.swing.JButton();
        addFileButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        //jTree1 = new javax.swing.JTree();
        addFamilyMemberButton = new javax.swing.JButton();
        removeFamilyMemberButton2 = new javax.swing.JButton();
        viewPersonalInfoButton = new javax.swing.JButton();
        removeFamilyMemberButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 255));

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        switchClientButton.setBackground(new java.awt.Color(255, 255, 255));
        switchClientButton.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        switchClientButton.setText("Switch Clients");

        exitButton.setBackground(new java.awt.Color(255, 255, 255));
        exitButton.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        exitButton.setText("Exit");

        saveAsButton2.setBackground(new java.awt.Color(255, 255, 255));
        saveAsButton2.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        saveAsButton2.setText("Save As");

        createClientButton.setBackground(new java.awt.Color(255, 255, 255));
        createClientButton.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        createClientButton.setText("Create Client");

        addFileButton1.setBackground(new java.awt.Color(255, 255, 255));
        addFileButton1.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        addFileButton1.setText("Add File");

        treePanel.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        treePanel.setForeground(new java.awt.Color(255, 255, 255));
        treePanel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        treePanel.setFocusable(false);
        jScrollPane1.setViewportView(treePanel);

        addFamilyMemberButton.setBackground(new java.awt.Color(255, 255, 255));
        addFamilyMemberButton.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        addFamilyMemberButton.setText("Add Family Member");

        removeFamilyMemberButton2.setBackground(new java.awt.Color(255, 255, 255));
        removeFamilyMemberButton2.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        removeFamilyMemberButton2.setText("Remove Family Member");

        viewPersonalInfoButton.setBackground(new java.awt.Color(255, 255, 255));
        viewPersonalInfoButton.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        viewPersonalInfoButton.setText("View Personal Information");

        removeFamilyMemberButton1.setBackground(new java.awt.Color(255, 255, 255));
        removeFamilyMemberButton1.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        removeFamilyMemberButton1.setText("Remove Family Member");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1086, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(viewPersonalInfoButton)
                                                        .addComponent(removeFamilyMemberButton1)
                                                        .addComponent(removeFamilyMemberButton2)
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(saveAsButton2)
                                                                        .addComponent(addFamilyMemberButton))
                                                                .addGap(18, 18, 18)
                                                                .addComponent(exitButton))))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(32, 32, 32)
                                                .addComponent(createClientButton)
                                                .addGap(18, 18, 18)
                                                .addComponent(addFileButton1)
                                                .addGap(18, 18, 18)
                                                .addComponent(switchClientButton)))
                                .addContainerGap(72, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(createClientButton)
                                        .addComponent(addFileButton1)
                                        .addComponent(switchClientButton)
                                        .addComponent(saveAsButton2)
                                        .addComponent(exitButton))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(180, 180, 180)
                                                .addComponent(addFamilyMemberButton)
                                                .addGap(44, 44, 44)
                                                .addComponent(removeFamilyMemberButton2)
                                                .addGap(49, 49, 49)
                                                .addComponent(viewPersonalInfoButton)
                                                .addGap(51, 51, 51)
                                                .addComponent(removeFamilyMemberButton1)
                                                .addContainerGap(455, Short.MAX_VALUE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addComponent(jScrollPane1)
                                                .addContainerGap())))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();

        createClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //check if tree is not saved and reset the main variables

                System.out.println(currentTree);
                //currentTree = new FamilyTree();
                displayTree(currentTree);

            }

        });

        addFileButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayTree(currentTree);
            }
        });

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int dialogResult = JOptionPane.showConfirmDialog(frame, "Are you sure you want to exit? Any unsaved data will be lost.");
                if (dialogResult == JOptionPane.YES_OPTION) {

                    System.exit(0);
                } else if (dialogResult == JOptionPane.NO_OPTION) {

                    return;
                }

            }
        });

        viewPersonalInfoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                FamilyMember member = new FamilyMember("Jane doe", "12,12,12", "alive");

                FamilyMember child = new FamilyMember("child doe", "12,12,12", "alive");

                FamilyMember husband = new FamilyMember("husband doe", "12,12,12", "alive");

                FamilyMember parent1 = new FamilyMember("Parent11 doe", "12,12,12", "alive");

                FamilyMember parent2 = new FamilyMember("Parent22 doe", "12,12,12", "alive");

                member.addChild(child);

                member.addSpouse(husband);

                member.addParent(parent1);

                member.addParent(parent2);

                displayInfo(member);

            }
        });

    }// </editor-fold>

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Masino_VanHecke_Project4.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Masino_VanHecke_Project4.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Masino_VanHecke_Project4.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Masino_VanHecke_Project4.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Masino_VanHecke_Project4().setVisible(true);

                } catch (IOException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class
                            .getName()).log(Level.SEVERE, null, ex);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }

    // Variables declaration - do not modify
    private javax.swing.JButton addFamilyMemberButton;
    private javax.swing.JButton exitButton;
    private javax.swing.JButton addFileButton1;
    private javax.swing.JButton createClientButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    //private javax.swing.JTree jTree1;
    private javax.swing.JButton viewPersonalInfoButton;
    private javax.swing.JButton removeFamilyMemberButton1;
    private javax.swing.JButton removeFamilyMemberButton2;
    private javax.swing.JButton saveAsButton2;
    private javax.swing.JButton switchClientButton;
    // End of variables declaration

    /*
    public void saveChart(JFreeChart workingChart) {


        //Makes two boolean variables and sets them to false
        Boolean overWrite = false;
        Boolean overWriteNecessary = false;
        //Creates the width and height of the png file we want to download to the
        //computer
        int option;
        int width = 640;
        int height = 480;

        //https://www.youtube.com/watch?v=rEsHS0ov3fw&ab_channel=BrandonGrasley
        //Used above link to help me with actually making the save function work
        JFileChooser saveChooser = new JFileChooser();

        //When the saveButton is clicked, then the user's directory will pop up
        saveChooser.setCurrentDirectory(new File("C:\\"));

        //Users can choose the extension file name they'd like to give to
        //the image they are saving
        saveChooser.setFileFilter(new FileNameExtensionFilter("PNG images", "png", "jpeg", "jpg"));

        //Finds the place where the user would like to save the image and
        //then returns the index of that location
        int saveResult = saveChooser.showSaveDialog(null);

        //This gets the file name that was written by the user and stores
        //it in a File object called savedFile
        File savedFile = saveChooser.getSelectedFile();

        //If the user approves to download a specific chart to their computer
        //then the program will enter this if statement
        if (saveResult == JFileChooser.APPROVE_OPTION) {

            try {//Try statement

                //https://www.coderanch.com/t/334281/java/JFileChooser-overwrite-file
                //Used the above link to try and determine how to properly
                //overwrite an image and ask the user to confirm their decision
                //to do so

                //Enters the if statement if there is already another file
                //saved to the computer with the exact same name used
                if (savedFile.exists()) {

                    overWriteNecessary = true;

                    //Asks the user to confirm whether or not they want
                    //to overwrite the image
                    option = JOptionPane.showConfirmDialog(null, "A"
                            + " file with this name already exists, would you like"
                            + "to overwrite it?");

                    //If the user chooses "No" then the program will exit
                    //out of the method
                    if (option == JOptionPane.YES_OPTION) {

                        overWrite = true;
                    }//Ends nested if statement

                }//Ends if statement

                //Enters this if statement if the user agreed to overwrite the image
                //or if there wasn't an image with the same name present
                //in the computer's system
                if (overWrite == true || overWriteNecessary == false) {

                    //Saves image to the user's computer
                    ChartUtilities.saveChartAsPNG(saveChooser.getSelectedFile(), workingChart, width, height);

                }//Ends if statement

            } catch (IOException ex) {

                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);

            }//Ends catch

        }//Ends try statement

    }//Ends saveChart method
     */
    
        //create the root node
//        DefaultMutableTreeNode main = new DefaultMutableTreeNode("Main");
//        //last selected path to keep track of the last person the user selected.
//        //Used when adding or canceling an action
//        TreePath lastSelectedNode = null;
//
//        //mutable tree node allowing objects as nodes
//        DefaultMutableTreeNode top;
//
//        //no data loaded inthe tree
//        if (!familyTree.hasRoot()) {
//            top = new DefaultMutableTreeNode("No tree data found.");
//
//        } else {
//            //add the root person
//            top = new DefaultMutableTreeNode(familyTree.getRoot());
//            //call the recursive method to populate the entire tree with all the
//            //details from the root family member
//            createTree(top, familyTree.getRoot());
//            //if the user selected a member, set the last selected path
//            lastSelectedNode = jTree1.getSelectionPath();
//
//        }
//        //Create the tree and allow one selection at a time and hide the root node
//        jTree1 = new JTree(main);
//        main.add(top);
//        jTree1.setRootVisible(false);
//        jTree1.setShowsRootHandles(true);
//        jTree1.setEnabled(true);
//        jTree1.expandPath(new TreePath(main.getPath()));
//        jTree1.getSelectionModel().addTreeSelectionListener(new treeSelectorAction());
//        jTree1.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
//        jTree1.setBorder(new EmptyBorder(0, 10, 0, 10));
//
//        //expand all the tree nodes
//        for (int i = 0; i < jTree1.getRowCount(); i++) {
//            jTree1.expandRow(i);
//        }
//
//        //have a custom renderer for the nodes of the tree
//        //dim the text nodes and allow selection of the familymember object nodes
//        jTree1.setCellRenderer(new DefaultTreeCellRenderer() {
//            @Override
//            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean exp, boolean leaf, int row, boolean hasFocus) {
//
//                DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
//                Object nodeInfo = node.getUserObject();
//                if (nodeInfo instanceof FamilyMember) {
//                    setTextNonSelectionColor(Color.BLACK);
//                    setBackgroundSelectionColor(Color.LIGHT_GRAY);
//                    setTextSelectionColor(Color.BLACK);
//                    setBorderSelectionColor(Color.WHITE);
//                } else {
//                    setTextNonSelectionColor(Color.GRAY);
//                    setBackgroundSelectionColor(Color.WHITE);
//                    setTextSelectionColor(Color.GRAY);
//                    setBorderSelectionColor(Color.WHITE);
//                }
//                setLeafIcon(null);
//                setClosedIcon(null);
//                setOpenIcon(null);
//                super.getTreeCellRendererComponent(tree, value, sel, exp, leaf, row, hasFocus);
//                return this;
//            }
//        });
//
//        //add the tree to a scrolepane so the user can scroll
//        JScrollPane treeScrollPane = new JScrollPane(jTree1);
//        treeScrollPane.setPreferredSize(new Dimension(250, 0));
//
//        //scroll the tree to the last selected path
//        jTree1.setSelectionPath(lastSelectedNode);

}
