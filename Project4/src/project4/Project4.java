/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package project4;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author brijolievanhecke
 */
public class Project4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        /**!!A lot of this was taken from my second project
     * The purpose of this method is to give the user the ability to save the 
     * chart to their computers files
     *
     * @param workingChart
     */
        
        /*
    public void saveChart(JFreeChart workingChart) {

        
        //Makes two boolean variables and sets them to false
        Boolean overWrite = false;
        Boolean overWriteNecessary = false;
        //Creates the width and height of the png file we want to download to the
        //computer
        int option;
        int width = 640;
        int height = 480;

        //https://www.youtube.com/watch?v=rEsHS0ov3fw&ab_channel=BrandonGrasley
        //Used above link to help me with actually making the save function work
        JFileChooser saveChooser = new JFileChooser();

        //When the saveButton is clicked, then the user's directory will pop up
        saveChooser.setCurrentDirectory(new File("C:\\"));

        //Users can choose the extension file name they'd like to give to
        //the image they are saving
        saveChooser.setFileFilter(new FileNameExtensionFilter("PNG images", "png", "jpeg", "jpg"));

        //Finds the place where the user would like to save the image and
        //then returns the index of that location
        int saveResult = saveChooser.showSaveDialog(null);

        //This gets the file name that was written by the user and stores
        //it in a File object called savedFile
        File savedFile = saveChooser.getSelectedFile();

        //If the user approves to download a specific chart to their computer
        //then the program will enter this if statement
        if (saveResult == JFileChooser.APPROVE_OPTION) {

            try {//Try statement

                //https://www.coderanch.com/t/334281/java/JFileChooser-overwrite-file
                //Used the above link to try and determine how to properly
                //overwrite an image and ask the user to confirm their decision
                //to do so
                
                //Enters the if statement if there is already another file
                //saved to the computer with the exact same name used
                if (savedFile.exists()) {

                    overWriteNecessary = true;

                    //Asks the user to confirm whether or not they want
                    //to overwrite the image
                    option = JOptionPane.showConfirmDialog(null, "A"
                            + " file with this name already exists, would you like"
                            + "to overwrite it?");

                    //If the user chooses "No" then the program will exit
                    //out of the method
                    if (option == JOptionPane.YES_OPTION) {

                        overWrite = true;
                    }//Ends nested if statement

                }//Ends if statement

                //Enters this if statement if the user agreed to overwrite the image
                //or if there wasn't an image with the same name present
                //in the computer's system
                if (overWrite == true || overWriteNecessary == false) {

                    //Saves image to the user's computer
                    ChartUtilities.saveChartAsPNG(saveChooser.getSelectedFile(), workingChart, width, height);

                }//Ends if statement

            } catch (IOException ex) {

                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);

            }//Ends catch

        }//Ends try statement

    }//Ends saveChart method
*/
    }
    
}
