/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package masino_vanhecke_project4;

import java.io.Serializable;
import java.util.HashMap;

/**
 * The purpose of this class is to manage the data properties and methods of
 * a family member object
 * @author brijolievanhecke
 */
public class FamilyMember implements Serializable {

     //The below declares the private data properties we will be using throughout the
    //rest of the class
    private String name;

    private String status;

    private String dateOfBirth;

    private Boolean isAlive;

    private HashMap<String, FamilyMember> parents;

    private HashMap<String, FamilyMember> children;

    private FamilyMember spouse;

    private Boolean spouseIsDisplayed;

    /**
     *This familymember constructor creates a familymember object and gives it a 
     * name, dob, and status (indicates if they are alive or living)> This constructor
     * also gives them an empty hashmap of children and parents
     * @param parent
     */
    FamilyMember(String name, String DOB, String Status) {

        //sets their data properties to whatever was passed into the parameters
        this.name = name;
        this.dateOfBirth = DOB;
        this.status = Status;
        
        //Creates an empty hashmap for parents and children
        this.parents = new HashMap<>();
        this.children = new HashMap<>();
        //Makes it so the spouse is not automatically displayed
        spouseIsDisplayed = false;
        
        //The below if statement reads in the status string passed into the parameter
        //and determines if the family member is alive or dead by looking for a comma
        //which separates residence. If the status is a residence then the
        //family member is declared to be alive, and vice versa
        if (status.contains(",")) {
            this.setStatus(true, status);
        } else {
            this.setStatus(false, status);

        }//Ends if stament

    }//Ends family member constructor method

    /**
     *The purpose of this method is to add a parent to a familymember's
     * personal parent hashmap
     * @param parent
     */
    public void addParent(FamilyMember parent) {
        
        parents.put(parent.getName(), parent); //Adds parent
    }//Ends addParent method

    /**
     *The purpose of this method is to set a family members spouse to whatever
     * family member they pass in
     * @param newSpouse
     */
    public void addSpouse(FamilyMember newSpouse) {
        spouse = newSpouse; //Sets new spouse
        
    }//Ends addSpouse method

    /**
     *The purpose of this method is to add a parent to a familymember's
     * personal children hashmap
     * @param child
     */
    public void addChild(FamilyMember child) {
        children.put(child.getName(), child);//Adds children
    }//ends addChild method

    /**
     *The purpose of this method is to convert a living person's status to dead
     * @param dateOfDeath
     */
    public void convertToDead(String dateOfDeath) {
        isAlive = false; //sets isalive boolean to false
        status = dateOfDeath;//Changes their status to a date of death
    }//ends convertToDead method

    /**
     *The purpose of this method is to remove a child from a familymember's
     * personal parent hashmap
     * @param childToRemove
     */
    public void removeChild(FamilyMember childToRemove) {
        String name = childToRemove.getName(); //Gets the name of the child we want
        //to remove
        children.remove(name); //Removes the familymember with that name from the children
        //hashmap
    }//ends removechild method

    /**
     *The purpose of this method is to clear all children from a family member's
     * personal hashmap
     */
    public void removeAllChildren() {
        children.clear(); //clears children hashmap
    }//Ends removeAllChildren
    
    /**
     *The purpose of this method is to clear all parents from a family member's
     * personal hashmap
     */
    public void removeAllParents(){
        parents.clear(); //Clears all parents from parents hashmap
    }//Ends removeAllAParents method

    /**
     *The purpose of this method is to remove a parent from a familymember's
     * personal parent hashmap
     * @param parentToRemove
     */
    public void removeParent(FamilyMember parentToRemove) {
        String name = parentToRemove.getName();//Gets the name of the parent we want
        //to remove
        
        parents.remove(name);//Removes the familymember with that name from the children
        //hashmap
    }//Ends removeParent method

    /**
     *The purpose of this method is to remove a spouse from a familymember's
     * personal parent hashmap
     */
    public void removeSpouse() {
        spouse = null; //Sets spouse equal to null
    }//Ends the rmeoveSpouse method

    /**
     *The purpose of this method is to return the children of a familymember
     * @return hashmap
     */
    public HashMap<String, FamilyMember> getChildren() {
        return children; //Returns children hashmap
    }//ends the getChildren method

    /**
     *The purpose of this method is to return the parents of a familymember
     * @return hashmap
     */
    public HashMap<String, FamilyMember> getParents() {
        return parents;//Returns parents hashmap
    }//Ends the getParents method

    /**
     *The purpose of this method is to return the spouse of a familymember
     * @return familymember
     */
    public FamilyMember getSpouse() {
        return spouse;//Returns spouse

    }//Ends getSpouse method

    /**
     *The purpose of this method is to return the name of a familymember
     *  @return string
     */
    public String getName() {
        return name;//Returns name
    }//Ends the getName method


    /**
     *The purpose of this method is to return the status of a familymember
     *  @return string
     */
    public String getStatus() {
        return status;//Returns status
    }//Ends the getStatus method

    /**
     *The purpose of this method is to set a family members status
     * @param boolean, String
     */
    public void setStatus(boolean choice, String status) {
        
        isAlive = choice; //Sets the isAlive boolean to the choice the user passed in

        this.status = status;//Changes the status of the familymember to the
        //status passed in as a parameter
        
    }//Ends the setStatus method

    /**
     *The purpose of this method is to get a familymembers dob
     * @return String
     */
    public String getDateOfBirth() {
        return dateOfBirth;//Returns dob

    }//Ends getdob metho

    /**
     *The purpose of this is to return whether the family member is alive or not
     * @return boolean
     */
    public Boolean checkIsAlive() {
        return isAlive;//retusn isalive
    }//Ends checkIsalive method

    /**
     *The purpose of this method is to set whether a family member is alive or not
     * @param boolean
     */
    public void setIsAlive(Boolean choice) {
        isAlive = choice;//Sets the isalive boolean to the user's choice

    }//Ends setIsAlive method


     /**
     *The purpose of this method is to get whether a family member's spouse is 
     * displayed or not
     * @return boolean
     */
    public Boolean checkSpouseIsDisplayed() {
        return spouseIsDisplayed;//Returns spouseIsDisplayed
    }//Ends checkSpouseIsDisplayed

    /**
     *The purpose of this method is to set whether a family member's spouse is 
     * displayed or not
     * @param boolean
     */
    public void setSpouseIsDisplayed(Boolean choice) {
        spouseIsDisplayed = choice;//Sets spouseIsDisplayed
    }//Ends setSpouseIsDisplayed

    /**
     *The purpose of this method is to return a tostring of the family member
     * @param boolean
     */
    @Override
    public String toString() {
        return "" + name + ", " + dateOfBirth + ", " + status; //Returns the specific
        //string format of the family member
    }//Ends tostring method of the familymember class

}//Ends familymember class
