/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package masino_vanhecke_project4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The purpose of this class is to manage the behavior of a clientnameslist txt
 * file that should be able store and write out the list of client names
 *
 * @author brijolievanhecke
 */
public class ClientFiles {

    /**
     *The purpose of this method is to read the list of client names being stored
     * in the clientnames txt file and populate the clientnames arraylist 
     * with the names being stored
     * @param clientNames
     * @throws IOException
     */
    public void readClientInfo(ArrayList<String> clientNames) throws IOException {

        //This looks for a file in our project with this file path
        String filePath = "src/clientInfo/clientNames";
        String line;
        //This creates a bufferedreaderobject that will read the information from the clientNames file
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        
        //Creates a while loop that wont break until there is no more information 
        //to parse through in the clientnames file
        while ((line = reader.readLine()) != null) {
            
            if (!(line.trim().isEmpty())) {//If statement that checks if the line is trimmed
                //or not
                
                clientNames.add(line);//Adds the line, which is a clientname,
                //to the clientnames list
                
            }//Ends if statement
        }//Ends while looop
    }//Ends readClientInfo method

    /**
     *The purpose of this method is to take a clientname from a client that was just 
     * created and to add that name to the clientnames txt file
     * @param string
     * @throws IOException
     */
    public void writeToClientInfoFile(String string) throws IOException {
         //This looks for a file in our project with this file path
        String filePath = "src/clientInfo/clientNames";
        //This creates a bufferedwriterobject that will write the information to the clientNames file
        BufferedWriter writer = new BufferedWriter(new FileWriter(filePath, true));
        //This appends the new clientname to the clientnames txt file
        writer.append(string);
        //Creates a new line
        writer.newLine();
        //closes the write object
        writer.close();
    }//Ends the writeToClientInfoFile
}//Ends the clientFiles class
