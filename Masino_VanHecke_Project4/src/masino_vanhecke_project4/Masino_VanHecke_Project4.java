/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package masino_vanhecke_project4;
//imports
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * This class creates a GUI and allows the user to manipulate and visualize family tree information
 * It is persistant between runs of the program and autosaves all changes
 * @author brijolievanhecke
 */
//General Structure and some methods (displaytree, createtree, etc) inspired by: 
//https://github.com/Taaqif/Family-Tree-Application/blob/master/TreeGUI.java
public class Masino_VanHecke_Project4 extends javax.swing.JFrame {

    //private fields 
    private HashMap<String, Client> clientsHash;
    private ClientFiles fc = new ClientFiles();
    private HashMap<String, FamilyMember> currentHashMap;
    private ArrayList<String> clientNames;
    private Client currentClient;
    private FamilyMember god;
    private JTree tree;
    private DefaultTreeModel dTree;
    private JFrame frame = this.frame;
    private JPanel treePanel;

    //private GUI components
    private javax.swing.JLabel DOBLabel2;
    private javax.swing.JLabel piblingsLabel;
    private javax.swing.JTextField piblingsTextField;
    private javax.swing.JLabel childrenLabel;
    private javax.swing.JTextField childrenTextField;
    private javax.swing.JTextField dobTextField;
    private javax.swing.JLabel grandChildrenLabel1;
    private javax.swing.JTextField grandChildrenTextField1;
    private javax.swing.JLabel grandParentsLabel;
    private javax.swing.JTextField grandParentsTextField;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nameLabel1;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JLabel niblingLabel;
    private javax.swing.JTextField niblingTextField;
    private javax.swing.JLabel parentsLabel1;
    private javax.swing.JTextField parentsTextField1;
    private javax.swing.JLabel personalInformationLabel;
    private javax.swing.JPanel personalInformationPanel;
    private javax.swing.JLabel spouseLabel;
    private javax.swing.JTextField spouseTextField;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JTextField statusTextField;
    private javax.swing.JPanel contentPane;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JMenu jMenu;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar;

    /*
    A method that adds a new file to the current client being displayed
    */
    private void addFile() throws IOException, FileNotFoundException, ClassNotFoundException {
        try {
            //Source: https://stackoverflow.com/questions/30176508/how-to-change-the-title-of-the-jfilechooser-dialogbox        
            //create a new jfilechooser to allow user to select the file they wish to add
            JFileChooser chooser = new JFileChooser();         
            try {
                //promp user
                chooser.setDialogTitle("Add file for " + currentClient.getName());
            }
            catch (NullPointerException ex) {
                //error handling for if there is no client loaded
                 JOptionPane.showMessageDialog(null, "No client loaded",
                    "!", JOptionPane.ERROR_MESSAGE);
                 return;
            }
            chooser.setCurrentDirectory(new File("C:\\"));
            //the result of their choice
            int result = chooser.showSaveDialog(null);
            //if they confirm their choice
            if (result == JFileChooser.APPROVE_OPTION) {
                //create a file with that file path
                File newfile = chooser.getSelectedFile();   
                //call merge on the currentclient using the newfile
                currentClient.merge(newfile);
                //write the client information to the client's file to save changes
                currentClient.writeToFile();
                //display the tree
                displayTree(god);
                //display the current client
                try {
                    jMenu4.setText("Current Client is: " + currentClient.getName());
                } catch (NullPointerException e) {
                    //if no client is displayed then clear it
                    jMenu4.setText("Current Client is: " + "");
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            //catch if the format for a file is incorrect
            JOptionPane.showMessageDialog(null, "Invalid file format",
                    "!", JOptionPane.ERROR_MESSAGE);

        }

    }

    /*
    this method displays the personal information of a family member chosen by
    the user in a dropdown
    */
    private void viewPersonalInfo() {
        try {
            //create namesList and add all the keys of the currenthashmap (member names)
            //in order to populate the dropdown options
            ArrayList<String> namesList = new ArrayList<>();
            //Source: https://www.geeksforgeeks.org/how-to-iterate-hashmap-in-java/
            for (Map.Entry<String, FamilyMember> set
                    : currentHashMap.entrySet()) {
                String name = set.getKey();
                namesList.add(name);
            }
            //change the list to an array in order to pass it into the joptionpane
            String[] array = namesList.toArray(new String[0]);
            //if there are no family members added pop up a message and quit the action
            if (array.length == 0) {
                JOptionPane.showMessageDialog(null, "No Family Members to choose from",
                        "!", JOptionPane.ERROR_MESSAGE);
                return;
                //if there is at least one family member then allow the user to choose from the 
                //family member list
            } else if (array.length >= 1) {
                try {
                    String input = (String) JOptionPane.showInputDialog(null, "Choose a family member",
                            "", JOptionPane.QUESTION_MESSAGE, null, // Use default icon
                            array, // Array of choices
                            array[1]); // Initial choice 
                    //check the hashmap for the key that matches the input and display the
                    //information of the family member that matches
                    for (Map.Entry<String, FamilyMember> set
                            : currentHashMap.entrySet()) {
                        if (input.equals(set.getKey())) {
                            FamilyMember member = set.getValue();
                            displayInfo(member);
                        }
                    }
                } catch (NullPointerException e) {
                }
            }
            //if there are no clients then pop up a message
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "No clients have been added",
                    "!", JOptionPane.ERROR_MESSAGE);
        }

    }

    /*
    clears the textfields of the member information panel 
    */
    private void clearTextFields() {
        statusTextField.setText("");
        statusTextField.setText("");
        spouseTextField.setText("");
        childrenTextField.setText("");
        parentsTextField1.setText("");
        grandParentsTextField.setText("");
        piblingsTextField.setText("");
        grandChildrenTextField1.setText("");
        niblingTextField.setText("");
        nameTextField.setText("");
        dobTextField.setText("");
    }

    /*
    removes a family member chosen from a dropdown
    */
    private void removeFamilyMember() throws IOException, FileNotFoundException, ClassNotFoundException {
       //again create an arraylist of names and then convert it to an array to display the 
       //options, this time you check the family members
        ArrayList<String> namesList = new ArrayList<>();
        try {
            //add all the names from the hashmap to the name list
            for (Map.Entry<String, FamilyMember> set : currentHashMap.entrySet()) {
                String name = set.getKey();
                namesList.add(name);
            }         
            String[] array = namesList.toArray(new String[0]);
            //if there are no names prompt the user
            if (array.length == 0) {
                JOptionPane.showMessageDialog(null, "No Family Members to choose from",
                        "!", JOptionPane.ERROR_MESSAGE);
                return;
                //if there are names then get the user choice as input
            } else if (array.length >= 1) {
                try {
                    String input = (String) JOptionPane.showInputDialog(null, "Please select the individual you want to remove",
                            "", JOptionPane.QUESTION_MESSAGE, null, // Use
                            // default
                            // icon
                            array, // Array of choices
                            array[1]); // Initial choice 
                    //find the member in the current hash mao that matches the input key
                    FamilyMember member = currentHashMap.get(input);  
                    //if that member has a spouse
                    if (member.getSpouse() != null) {     
                        //remove the member from thier spouse's spouse field
                        currentHashMap.get(member.getSpouse().getName()).removeSpouse();
                    }
                    //if that member has parents
                    if (!member.getParents().isEmpty()) {
                        //remove the member from their parent's child hashmap
                        //done for each parent
                        for (Map.Entry<String, FamilyMember> set2 : member.getParents().entrySet()) {
                            FamilyMember temp = set2.getValue();
                            currentHashMap.get(temp.getName()).removeChild(member);
                        }
                    }
                    //if that member has children
                    if (!member.getChildren().isEmpty()) {
                        //remove the member from their children's parent hashmap
                        //done for each child
                        for (Map.Entry<String, FamilyMember> set2 : member.getChildren().entrySet()) {
                            FamilyMember temp2 = set2.getValue();
                            currentHashMap.get(temp2.getName()).removeParent(member);
                        }
                    }
                    //remove the family member in the hashmap that matches the key
                    currentHashMap.remove(member.getName());
                    //write the new hashmap to the client's file
                    currentClient.writeToFile();
                    //read the old file to update
                    currentClient.readOldFile();
                    //display the tree again
                    displayTree(god);

                } catch (NullPointerException e) {
                }

            }
            //prompt user if there are no clients
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "No clients have been added",
                    "!", JOptionPane.ERROR_MESSAGE);
        }
    }

    /*
    makes a family member deceased by prompting the user to type a date of death
    */
    private void makeDeceased() throws IOException, FileNotFoundException, ClassNotFoundException {
        try {
            //same as before we are populate the user's options with the family member names
            ArrayList<String> namesList = new ArrayList<>();
            for (Map.Entry<String, FamilyMember> set : currentHashMap.entrySet()) {
                String name = set.getKey();
                namesList.add(name);
            }
            String[] array = namesList.toArray(new String[0]);
            //make sure there is a family member to choose from
            if (array.length == 0) {
                JOptionPane.showMessageDialog(null, "No Family Members to choose from",
                        "!", JOptionPane.ERROR_MESSAGE);
                return;
            } else if (array.length >= 1) {
                try {
                    String input = (String) JOptionPane.showInputDialog(null, "Please select family member who passed away",
                            "", JOptionPane.QUESTION_MESSAGE, null, // Use
                            // default
                            // icon
                            array, // Array of choices
                            array[1]); // Initial choice 
                    for (Map.Entry<String, FamilyMember> set
                            : currentHashMap.entrySet()) {
                        //check the hashmap keys and match the input to an entry
                        if (input.equals(set.getKey())) {
                            //make a member and set it to the value in the ahsmap
                            FamilyMember member = set.getValue();
                            //if the member is already deceased prompt the user
                            if (member.checkIsAlive() == false) {
                                JOptionPane.showMessageDialog(null, "Family member has already been marked as deceased",
                                        "!", JOptionPane.ERROR_MESSAGE);
                                //if the member is alive then prompt for a death date
                            } else if (member.checkIsAlive() == true) {
                                String dod = JOptionPane.showInputDialog(
                                        "Please enter client's date of death. Please enter date format as 'MM DD YYYY", null);
                                //source: https://stackoverflow.com/questions/28920898/how-to-check-if-there-is-a-space-between-two-characters
                                //if the date the enter matches the desired format
                                if (dod.matches("\\d{2} \\d{2} \\d{4}")) {
                                    //get the member in the hashmap that matches the input and set 
                                    //thier status to false with the date of death
                                    currentHashMap.get(input).setStatus(false, dod);
                                    //set their isalive field to false
                                    currentHashMap.get(input).setIsAlive(false);
                                    //write to the client's file
                                    currentClient.writeToFile();
                                    //read the cleint's file
                                    currentClient.readOldFile();
                                    //display the changed family member's information
                                    displayInfo(currentHashMap.get(input));
                                    //prompt that the format was incorrect
                                } else {
                                    JOptionPane.showMessageDialog(null, "Date of Death entered incorrectly. Please try again",
                                            "!", JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            //display the family members information if they are already dead
                            displayInfo(currentHashMap.get(input));
                        }
                    }
                } catch (NullPointerException e) {
                }
            }
            //prompt that there are no clients
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "No clients have been added",
                    "!", JOptionPane.ERROR_MESSAGE);
        }
    }

    /*
    exits the program
    */
    private void exit() {
        System.exit(0);

    }

    /*
    create a client with the user input from the joptionpane
    */
    private void createClient() throws IOException, FileNotFoundException, ClassNotFoundException {
        String name = JOptionPane.showInputDialog(
                "Please enter client's name", null);
        try {
            //prompt user if the client alrady exists
            if (clientNames.contains(name)) {
                JOptionPane.showMessageDialog(null, "Client name already in use. Please use other name",
                        "!", JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    //if the name is not a blank line
                    if (!name.equals("")) {   
                        //allow the user to choose a new file for this client
                        JFileChooser chooser = new JFileChooser();                     
                        chooser.setDialogTitle("Add Client Folder");
                        chooser.setCurrentDirectory(new File("C:\\"));
                        int result = chooser.showSaveDialog(null);
                        //if the user confirms thier choice
                        if (result == JFileChooser.APPROVE_OPTION) {
                            clearTextFields();
                            //set the new client to a client with the name chosen
                            Client newClient = new Client(name);    
                            //put the client in the client hash
                            clientsHash.put(name, newClient);
                            currentClient = newClient;
                            //clear all the text fields       
                            File newfile = chooser.getSelectedFile();
                            //set the currenthashmap to the hashmap returned from the readnewfile 
                            //with new file
                            currentHashMap = newClient.readNewFile(newfile);
                            //write to the client's file and to the client info file
                            newClient.writeToFile();
                            fc.writeToClientInfoFile(name);
                            //add the client name to client names and set the current
                            //client to this nmew client
                            clientNames.add(name);
                            
                            //display the tree again
                            displayTree(god);
                            try {
                                //change the label for the jmenubaritem
                                jMenu4.setText("Current Client is: " + currentClient.getName());
                            } catch (NullPointerException e) {
                                //error handling
                                jMenu4.setText("Current Client is: " + "");
                            }
                        }
                    } else {
                        //error handling
                        JOptionPane.showMessageDialog(null, "Please enter valid name",
                                "!", JOptionPane.ERROR_MESSAGE);
                    }
                    //error handling
                } catch (ArrayIndexOutOfBoundsException e) {
                    JOptionPane.showMessageDialog(null, "Invalid file format",
                            "!", JOptionPane.ERROR_MESSAGE);
                }
            }
            //error handling
        } catch (NullPointerException e) {
        }
    }

    /*
    switches the client to a preexisting client
    */
    private void switchClient() throws IOException, FileNotFoundException, ClassNotFoundException {
        //give the user a list of client names to choose from in the joptionpane
        String[] array = clientNames.toArray(new String[0]);
        try {
            if (array.length >= 2) {
                String input = (String) JOptionPane.showInputDialog(null, "Choose a client",
                        "", JOptionPane.QUESTION_MESSAGE, null, // Use
                        // default
                        // icon
                        array, // Array of choices
                        array[1]); // Initial choice 
                currentClient = new Client(input);             
                currentClient.readOldFile();
                currentClient.writeToFile();
                currentHashMap = currentClient.getHashMap();
                displayTree(god);
            } else if (clientNames.size() == 0) {
                //error handling
                JOptionPane.showMessageDialog(null, "No Clients added. Please add client",
                        "!", JOptionPane.ERROR_MESSAGE);
                return;
                //error handling
            } else if (clientNames.size() == 1) {
                JOptionPane.showMessageDialog(null, "Only one client has been added to the program. Please add more than"
                        + " one client to view other Family Trees",
                        "!", JOptionPane.ERROR_MESSAGE);
                return;
            }
            //error handling
        } catch (NullPointerException e) {
        }
        //reset the jmenubaritem label
        try {
            jMenu4.setText("Current Client is: " + currentClient.getName());
        } catch (NullPointerException e) {
            jMenu4.setText("Current Client is: " + "");

        }

    }

    /*
    displays the tree given the root family member
    */
    private void displayTree(FamilyMember god) {
        //clear all the textfields
        clearTextFields();
        //Source: https://stackoverflow.com/questions/12697650/how-do-i-clear-a-jtree-modelremoving-all-nodes
       //set the tree to null
        tree.setModel(null);
        //clear the god nodes children and then add back any family members with no parents
        god.removeAllChildren();
        for (Map.Entry<String, FamilyMember> set : currentHashMap.entrySet()) {
            set.getValue().setSpouseIsDisplayed(false);
            if (set.getValue().getParents().isEmpty()) {
                god.addChild(set.getValue());
            }
        }

        //create the top defaultmutabletreenode using the god name
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(god.getName());
        // the above line is where we set it to the god node

        //call create tree with the rootnode and god
        createTree(rootNode, god);
        //make dtree a new defaulttreemodel with rootnode
        dTree = new DefaultTreeModel(rootNode);
        //make tree use dtree as the treemodel
        tree = new JTree(dTree);
        //tree formatting
        tree.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        tree.setForeground(new java.awt.Color(255, 255, 255));
        tree.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tree.setFocusable(false);
        tree.setSize(100, 100);

        //Jscroll formatting and size
        //source: https://stackoverflow.com/questions/10346449/scrolling-a-jpanel
        JScrollPane scrollP = new JScrollPane(tree);
        scrollP.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        scrollP.setForeground(new java.awt.Color(255, 255, 255));
        scrollP.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        scrollP.setFocusable(false);
        scrollP.setSize(100, 100);
        //add tree to the scrollpane viewport
        scrollP.setViewportView(tree);
        //add scrollp and tree to treepanel
        treePanel.add(scrollP);
        treePanel.add(tree);
    }

   

    /**
     * Recursive method to populate the jtree object for each family member of
     * the root person
     *
     * @param top the node to populate
     * @param root the member to get the details from
     */
    //source: https://www.decodejava.com/java-jtree.htm
    private void createTree(DefaultMutableTreeNode top, FamilyMember root) {
        //set default values to null
        DefaultMutableTreeNode spouseNode = null;
        DefaultMutableTreeNode spouse = null;
        DefaultMutableTreeNode children = null;
        DefaultMutableTreeNode child = null;
        //if the root has a spouse, create a dropdown called spouse and display the the spouse
        if (root.getSpouse() != null) {
            //check that display spouse is false
            if (root.getSpouse().checkSpouseIsDisplayed() == false) {
                spouseNode = new DefaultMutableTreeNode("Spouse");
                spouse = new DefaultMutableTreeNode(root.getSpouse().getName());
                //set the isdisplayed of the spouse to true
                root.getSpouse().setSpouseIsDisplayed(true);
                //add those nodes to the tree
                spouseNode.add(spouse);
                top.add(spouseNode);
            }

        }
        //if the root has children and it is the god node then skip adding
        //a child label and just add the children
        HashMap<String, FamilyMember> childrenList = root.getChildren();
        int ChildrenSize = childrenList.size();
        if (ChildrenSize > 0) {
            if (root.getDateOfBirth().equals("")) {
                for (Map.Entry<String, FamilyMember> set : childrenList.entrySet()) {
                    FamilyMember value = set.getValue();
                    child = new DefaultMutableTreeNode(value.getName());
                    createTree(child, value);
                    top.add(child);
                }
                //if root is not the god node then add a children label and then add the children
            } else {
                children = new DefaultMutableTreeNode("Children");
                for (Map.Entry<String, FamilyMember> set : childrenList.entrySet()) {
                    FamilyMember value = set.getValue();
                    child = new DefaultMutableTreeNode(value.getName());
                    createTree(child, value);
                    children.add(child);
                }
                top.add(children);
            }
        }
    }

    /*
    displays the information of a familymember passed in
    */
    private void displayInfo(FamilyMember member) {
        //set format for all the joptionpanes
        UIManager.put("OptionPane.minimumSize", new Dimension(500, 500));
        UIManager.put("OptionPane.messageFont", new Font("Arial", Font.BOLD, 18));
        //clear the text fields
        clearTextFields();
        //make a hashmap to store the grandparents and clear it 
        HashMap<String, FamilyMember> grandparents = new HashMap<String, FamilyMember>();
        grandparents.clear();
        //for every parent of the member, get thier parent
        for (Map.Entry<String, FamilyMember> entry : member.getParents().entrySet()) {
            FamilyMember parent = entry.getValue();
            //then get the values of the grnadparents and add them to the grandparent hash
            for (Map.Entry<String, FamilyMember> entryG : parent.getParents().entrySet()) {
                FamilyMember grandparent = entryG.getValue();
                grandparents.put(grandparent.getName(), grandparent);
            }
        }
        
        //make a hashmap to store the piblings and clear it
        HashMap<String, FamilyMember> piblings = new HashMap<String, FamilyMember>();
        piblings.clear();
        //for every parent the member has get thier parent
        for (Map.Entry<String, FamilyMember> entry : member.getParents().entrySet()) {
            FamilyMember parent = entry.getValue();
            for (Map.Entry<String, FamilyMember> entryG : parent.getParents().entrySet()) {
                FamilyMember grandparent = entryG.getValue();
                //get the children of the gradnparents and set them to be the piblings
                for (Map.Entry<String, FamilyMember> entryP : grandparent.getChildren().entrySet()) {
                    FamilyMember pibling = entryP.getValue();
                    if (!piblings.containsKey(pibling.getName())) {
                        piblings.put(pibling.getName(), pibling);
                    }
                }
            }
        }

        //create a hashmap for the grandchildren and clear it
        HashMap<String, FamilyMember> grandchildren = new HashMap<String, FamilyMember>();
        grandchildren.clear();
        //for every child the member has get the child
        for (Map.Entry<String, FamilyMember> entry : member.getChildren().entrySet()) {
            FamilyMember child = entry.getValue();
            //for every child get thier children and add them to the gradmnchildren hash
            for (Map.Entry<String, FamilyMember> entryZ : child.getChildren().entrySet()) {
                FamilyMember grandchild = entryZ.getValue();
                grandchildren.put(grandchild.getName(), grandchild);
            }
        }

        //create a nibling hashmap and clear it
        HashMap<String, FamilyMember> niblings = new HashMap<String, FamilyMember>();
        niblings.clear();
        //for every parent the member has 
        for (Map.Entry<String, FamilyMember> entry : member.getParents().entrySet()) {
            FamilyMember parent = entry.getValue();
            //for every child the parent has
            for (Map.Entry<String, FamilyMember> entryQ : parent.getChildren().entrySet()) {
                FamilyMember sibling = entryQ.getValue();
                //get the name of the niblings
                if (sibling.getName() != member.getName()) {
                    for (Map.Entry<String, FamilyMember> entryP : sibling.getChildren().entrySet()) {
                        FamilyMember nibling = entryP.getValue();
                        niblings.put(nibling.getName(), nibling);
                    }
                }
            }
        }

        //set the name field to the name of the member
        nameTextField.setText(member.getName());
        //set the date of birth field to the member dob
        dobTextField.setText(member.getDateOfBirth());

        //if the member is dead make the status label date of death, if not then make it date of death
        if (member.checkIsAlive().equals(false)) {
            statusLabel.setText("Date of Death");
        } else {
            statusLabel.setText("Residence");
        }

        //set the status field to the member's status
        statusTextField.setText(member.getStatus());
        //set all the relationship fields to the hashmaps created before stripped of thier brackets
        childrenTextField.setText(member.getChildren().keySet().toString().replaceAll("[\\[\\]]", ""));
        parentsTextField1.setText(member.getParents().keySet().toString().replaceAll("[\\[\\]]", ""));
        grandParentsTextField.setText(grandparents.keySet().toString().replaceAll("[\\[\\]]", ""));
        piblingsTextField.setText(piblings.keySet().toString().replaceAll("[\\[\\]]", ""));
        grandChildrenTextField1.setText(grandchildren.keySet().toString().replaceAll("[\\[\\]]", ""));
        niblingTextField.setText(niblings.keySet().toString().replaceAll("[\\[\\]]", ""));

        //https://stackoverflow.com/questions/11890585/is-it-possible-to-remove-square-brackets-from-immutablemultimap-keys
        //Above link helped me get rid of the brackets in the key values
        //sets spouse field to the name of the member's spouse 
        try {
            spouseTextField.setText(member.getSpouse().getName());

        } catch (NullPointerException e) {
            //remove the text if the spouse is null
            spouseTextField.removeAll();

        }

    }



    /**
     * Creates new form project4
     */
    public Masino_VanHecke_Project4() throws IOException, FileNotFoundException, ClassNotFoundException {
        //call initcomponents to boot up the gui
        initComponents();
        //set the
        //setSize(getMaximumSize());
        //read in the clientnames file information
        fc.readClientInfo(clientNames);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
// <editor-fold defaultstate="collapsed" desc="Generated Code">  
    private void initComponents() {

        //initialize global gui components
        tree = new JTree();
        god = new FamilyMember("Family Tree                                                     ", "", "");
        god.removeAllChildren();
        jPanel1 = new javax.swing.JPanel();
        clientsHash = new HashMap<String, Client>();
        clientNames = new ArrayList<String>();
        treePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        personalInformationPanel = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        nameLabel1 = new javax.swing.JLabel();
        spouseLabel = new javax.swing.JLabel();
        DOBLabel2 = new javax.swing.JLabel();
        childrenLabel = new javax.swing.JLabel();
        parentsLabel1 = new javax.swing.JLabel();
        nameTextField = new javax.swing.JTextField();
        statusTextField = new javax.swing.JTextField();
        dobTextField = new javax.swing.JTextField();
        childrenTextField = new javax.swing.JTextField();
        spouseTextField = new javax.swing.JTextField();
        personalInformationLabel = new javax.swing.JLabel();
        piblingsLabel = new javax.swing.JLabel();
        parentsTextField1 = new javax.swing.JTextField();
        grandParentsTextField = new javax.swing.JTextField();
        grandParentsLabel = new javax.swing.JLabel();
        piblingsTextField = new javax.swing.JTextField();
        grandChildrenLabel1 = new javax.swing.JLabel();
        niblingLabel = new javax.swing.JLabel();
        grandChildrenTextField1 = new javax.swing.JTextField();
        niblingTextField = new javax.swing.JTextField();
        contentPane = new javax.swing.JPanel();
        scrollPane = new javax.swing.JScrollPane();
        jMenuBar = new javax.swing.JMenuBar();
        jMenu = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();;
        jMenu3 = new javax.swing.JMenu();

        //Makes it so that the gui will end when the exit button is clicked
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        //Sets formatting of the jFrame
        setBackground(new java.awt.Color(204, 204, 255));

        //!!!!!
        
        //All of the below code deals with the formatting of various
        //gui components that will be placed on the gui
        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        treePanel.setFont(new java.awt.Font("Avenir Next Condensed", 0, 24)); // NOI18N
        treePanel.setForeground(new java.awt.Color(255, 255, 255));
        treePanel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        treePanel.setFocusable(false);

        treePanel.setSize(970, 500);
        jScrollPane1.setViewportView(treePanel);
        jScrollPane1.setSize(970, 500);

        personalInformationPanel.setBackground(new java.awt.Color(153, 153, 255));
        personalInformationPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        personalInformationPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        statusLabel.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        statusLabel.setForeground(new java.awt.Color(255, 255, 255));
        statusLabel.setText("Status:");

        nameLabel1.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        nameLabel1.setForeground(new java.awt.Color(255, 255, 255));
        nameLabel1.setText("Name:");

        spouseLabel.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        spouseLabel.setForeground(new java.awt.Color(255, 255, 255));
        spouseLabel.setText("Spouse:");

        DOBLabel2.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        DOBLabel2.setForeground(new java.awt.Color(255, 255, 255));
        DOBLabel2.setText("DOB:");

        childrenLabel.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        childrenLabel.setForeground(new java.awt.Color(255, 255, 255));
        childrenLabel.setText("Children:");

        parentsLabel1.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        parentsLabel1.setForeground(new java.awt.Color(255, 255, 255));
        parentsLabel1.setText("Parents:");

        nameTextField.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        nameTextField.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        nameTextField.setEditable(false);

        statusTextField.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        statusTextField.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        statusTextField.setEditable(false);

        dobTextField.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        dobTextField.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        dobTextField.setSize(new java.awt.Dimension(0, 40));
        dobTextField.setEditable(false);

        childrenTextField.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        childrenTextField.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        childrenTextField.setEditable(false);

        spouseTextField.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        spouseTextField.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        spouseTextField.setEditable(false);

        personalInformationLabel.setFont(new java.awt.Font("Avenir Next", 1, 32)); // NOI18N
        personalInformationLabel.setForeground(new java.awt.Color(255, 255, 255));
        personalInformationLabel.setText("Personal Information");
        personalInformationLabel.setToolTipText("Only biological familial relationships will be displayed");

        piblingsLabel.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        piblingsLabel.setForeground(new java.awt.Color(255, 255, 255));
        piblingsLabel.setText("Piblings");
        piblingsLabel.setToolTipText("Piblings are the aunts and uncles of an individual");

        parentsTextField1.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        parentsTextField1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        parentsTextField1.setEditable(false);

        grandParentsTextField.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        grandParentsTextField.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        grandParentsTextField.setEditable(false);

        grandParentsLabel.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        grandParentsLabel.setForeground(new java.awt.Color(255, 255, 255));
        grandParentsLabel.setText("Grandparents");

        piblingsTextField.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        piblingsTextField.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        piblingsTextField.setEditable(false);
        piblingsTextField.setToolTipText("Piblings are the aunts and uncles of an individual");

        grandChildrenLabel1.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        grandChildrenLabel1.setForeground(new java.awt.Color(255, 255, 255));
        grandChildrenLabel1.setText("GrandChildren");

        niblingLabel.setFont(new java.awt.Font("Avenir Next", 1, 18)); // NOI18N
        niblingLabel.setForeground(new java.awt.Color(255, 255, 255));
        niblingLabel.setText("Niblings");
        niblingLabel.setToolTipText("Niblings are the nieces and nephews of an individual");

        grandChildrenTextField1.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        grandChildrenTextField1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        grandChildrenTextField1.setEditable(false);

        niblingTextField.setFont(new java.awt.Font("Avenir Next", 1, 14)); // NOI18N
        niblingTextField.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        niblingTextField.setEditable(false);
        niblingTextField.setToolTipText("Niblings are the nieces and nephews of an individual");
        
        
        
        //!!
        
        //All of the below code is the gui generated code that places specific
        //components in different places 
        javax.swing.GroupLayout personalInformationPanelLayout = new javax.swing.GroupLayout(personalInformationPanel);
        personalInformationPanel.setLayout(personalInformationPanelLayout);
        personalInformationPanelLayout.setHorizontalGroup(
                personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(personalInformationPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(personalInformationPanelLayout.createSequentialGroup()
                                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(grandChildrenLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(grandParentsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(grandChildrenTextField1)
                                                        .addComponent(grandParentsTextField)))
                                        .addGroup(personalInformationPanelLayout.createSequentialGroup()
                                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                .addComponent(piblingsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(childrenLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(parentsLabel1, javax.swing.GroupLayout.Alignment.LEADING))
                                                        .addComponent(niblingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(childrenTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(parentsTextField1, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(piblingsTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(niblingTextField, javax.swing.GroupLayout.Alignment.TRAILING)))
                                        .addGroup(personalInformationPanelLayout.createSequentialGroup()
                                                .addComponent(personalInformationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 201, Short.MAX_VALUE))
                                        .addGroup(personalInformationPanelLayout.createSequentialGroup()
                                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(nameLabel1)
                                                        .addComponent(DOBLabel2)
                                                        .addComponent(statusLabel)
                                                        .addComponent(spouseLabel))
                                                .addGap(12, 12, 12)
                                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(spouseTextField)
                                                        .addComponent(dobTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(nameTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(statusTextField, javax.swing.GroupLayout.Alignment.TRAILING))))
                                .addContainerGap())
        );
        personalInformationPanelLayout.setVerticalGroup(
                personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(personalInformationPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(personalInformationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(nameLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(nameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(DOBLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(dobTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(statusTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(spouseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(spouseTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(childrenLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(childrenTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(parentsLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(parentsTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(piblingsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(piblingsTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(niblingLabel)
                                        .addComponent(niblingTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(grandChildrenLabel1)
                                        .addComponent(grandChildrenTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(personalInformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(grandParentsLabel)
                                        .addComponent(grandParentsTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        
        
        contentPane.setBackground(new java.awt.Color(153, 153, 255));
        contentPane.setForeground(new java.awt.Color(153, 153, 255));

        //Gives the scrollPane a horizontal and vertical scrollbar
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        treePanel.setBackground(new java.awt.Color(153, 153, 255));
        treePanel.setForeground(new java.awt.Color(153, 153, 255));

        //This makes the treePanel have a scrollPane
        scrollPane.setViewportView(treePanel);

        //The below is more generated code by the guibuilder
        javax.swing.GroupLayout contentPaneLayout = new javax.swing.GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
                contentPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(scrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 592, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(9, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
                contentPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(scrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 617, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(396, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap(12, Short.MAX_VALUE)
                                .addComponent(contentPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(personalInformationPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1286, 1286, 1286))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(contentPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(personalInformationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        
        //Ends of generated gui builder code

        //The belowe customizes the JmenuBar and the items on it
        jMenu.setText("Client");
        jMenuBar.add(jMenu);

        jMenu3.setText("Family Tree");
        jMenuBar.add(jMenu3);

        jMenu2.setText("File");
        jMenuBar.add(jMenu2);
        JMenuItem createClient = new JMenuItem("Create Client");
        jMenu.add(createClient);

        JMenuItem switchClient = new JMenuItem("Switch Client");
        jMenu.add(switchClient);

        JMenuItem exitAction = new JMenuItem("Exit");
        jMenu2.addSeparator();
        jMenu2.add(exitAction);

        JMenuItem addFile = new JMenuItem("Add File");
        jMenu3.add(addFile);
        jMenu3.addSeparator();

        JMenuItem removeFamilyMember = new JMenuItem("Remove Family Member");
        JMenuItem makeDeceased = new JMenuItem("Make a Family Member deceased");
        JMenuItem viewInfo = new JMenuItem("View a Family Member's Personal Information");

        jMenu3.add(removeFamilyMember);
        jMenu3.add(makeDeceased);
        jMenu3.add(viewInfo);

        jMenu4.setText("Current Client is: ");
        jMenuBar.add(jMenu4);

        
        //This calls the createClient() method when the createClient jmenuitem is
        //clicked on
        createClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    createClient();
                } catch (IOException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        //This calls the addFile() method when the addFile jmenuitem is
        //clicked on
        addFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    try {
                        addFile();
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        });

        //This calls the makeDeceased() method when the makeDeceased jmenuitem is
        //clicked on
        makeDeceased.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    makeDeceased();
                } catch (IOException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        //This calls the viewpersonalinfo() method when the view information jmenuitem is
        //clicked on
        viewInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewPersonalInfo();
            }
        });

        //This calls the switchclient() method when the switch client jmenuitem is
        //clicked on
        switchClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    switchClient();
                } catch (IOException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        //This calls the exit() method when the exit jmenuitem is
        //clicked on
        exitAction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exit();
            }
        });

        //This calls the removeFamilyMember() method when the removeFamilyMember jmenuitem is
        //clicked on
        removeFamilyMember.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    removeFamilyMember();
                } catch (IOException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        //Sets the jmenu bar of the jframe to the jmenu bar we initalized and created
        setJMenuBar(jMenuBar);

        
        //More generated code from the guibuilder
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        
        //Ends generated code from the guibuilder

    }// </editor-fold>                        

    /** !!!!!
     * THIS CODE IS ALL GENERATED BY THE GUIBUILDER
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Masino_VanHecke_Project4.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Masino_VanHecke_Project4.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Masino_VanHecke_Project4.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Masino_VanHecke_Project4.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Masino_VanHecke_Project4().setVisible(true);

                } catch (IOException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class
                            .getName()).log(Level.SEVERE, null, ex);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Masino_VanHecke_Project4.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }//Ends main
}//Ends the masino_vanhecke_project4 class
