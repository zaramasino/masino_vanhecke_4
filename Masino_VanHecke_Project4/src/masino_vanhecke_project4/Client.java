/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package masino_vanhecke_project4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
//source: https://mkyong.com/java/how-to-read-and-write-java-object-to-a-file/

/**
 * The purpose of this class is to manage the data properties and the behavior
 * of a client
 * @author brijolievanhecke
 */
public class Client {

    //The below declares the private data properties we will be using throughout the
    //rest of the class
    private String name;
    private HashMap<String, FamilyMember> memberList;

    
    /**
     *This constructor creates a client object and initializes them with a name 
     * and a membersList that contains no familymembers just yet
     * @return
     */
    Client(String name) {

        this.name = name; //Sets name equal to the name you pass in
        this.memberList = null; //Tells the client object that they currently
        //have no familymembers associated with them

    }//Ends client constructor with only one parameter
    
   
    /**
     *This constructor creates a client object and initializes them with a name 
     * and a membersList that contains all of their specific family members
     * @return
     */
    Client(String name, HashMap<String, FamilyMember> members){
        this.name = name;//Sets name equal to the name you pass in
        this.memberList = members;//Stores the hashmap containing all of the family
        //members associated with this specific client in the membersList
    }//Ends client constructor with two parameters

    /**
     *Returns clients name
     * @return
     */
    public String getName(){
        return name;
    }//Ends getName method
    
    /**
     *Returns clients hashmap which contains all of their current familymembers
     * @return
     */
    public HashMap getHashMap(){
        return memberList;
    }//Ends getHashMap method
    
    /**
     *The following method, readNewFile, reads in a new file that the user selected
     * and parses the file's information. This method will split the file's information
     * and use those splits to create the family members and establish
     * the familial relationships between different members in the file
     * @param File  
     * @return HashMap
     */
    //Source: https://www.tutorialspoint.com/trim-a-string-in-java-to-remove-leading-and-trailing-spaces#:~:text=To%20remove%20leading%20and%20trailing%20spaces%20in%20Java%2C%20use%20the,leading%20or%20trailing%20white%20space.
    public HashMap readNewFile(File filepath) throws FileNotFoundException, IOException{
        
        HashMap<String, FamilyMember> test = new HashMap<String, FamilyMember>();
        //Creates a hashmap called test that we will be adding familyMembers too
        
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(filepath));
        //The above line created  a BufferedReader object that will read the information
        //of the file we passed into the method
        
        while ((line = reader.readLine()) != null) {//This is a while loop that doesn't break
            //until the file we passed in has no more information left to parse
            
            if (!(line.trim().isEmpty())) {//This if statement checks if the current
                //line being looked at is not empty
                
                if (line.contains("parentof")) {//This if statement checks if the 
                    //line contains "parentsof" which indicates there is a relationship
                    //between two familymembers
                    
                    String[] parts = line.split(",");//Splits the line being looked 
                    //at into two parts based on where there is a comma and stores
                    //those splits into a string array
                    
                    //Creates two temp familymember objects that store the 
                    //information of the familymember from the memberList hashmap that has 
                    //their same name
                    FamilyMember parent = test.get(parts[0].trim());
                    FamilyMember child = test.get(parts[2].trim());
                    
                    //Establishes the familial relationships/bonds between the two 
                    //familyMembers we just created
                    child.addParent(parent);
                    parent.addChild(child);
                    
                } else if (line.contains("marriedto")) {//This if statement checks if the 
                    //line contains "marriedto" which indicates there is a relationship
                    //between two familymembers
                    
                    String[] parts = line.split(",");;//Splits the line being looked 
                    //at into two parts based on where there is a comma and stores
                    //those splits into a string array
                    
                    //Creates two temp familymember objects that store the 
                    //information of the familymember from the memberList hashmap that has 
                    //their same name
                    FamilyMember spouse1 = test.get(parts[0].trim());
                    FamilyMember spouse2 = test.get(parts[2].trim());
                    
                    //Establishes the familial relationships/bonds between the two 
                    //familyMembers we just created
                    spouse1.addSpouse(spouse2);
                    spouse2.addSpouse(spouse1);

                } else {//This if statement checks if the line being looked at doesn't
                    //store any specific key phrase we were previously looking for
                    //in other if statements, which indicates the file is
                    // declaring the actual family member
                    String[] parts = line.split(",", 3);
                    FamilyMember newMember = new FamilyMember(parts[0].trim(), parts[1].trim(), parts[2].trim());
                    test.put(parts[0].trim(), newMember);
                }//Ends if statement
                
            }//Ends if statement

        }//Ends while loop
        
        this.memberList = test; //This takes all of the familymembers we added
        //to the test hashmap and stores them in the client's personal memberList
        //hashMap
        reader.close(); //This closes the buffered reader object
        
        return test;//Returns hashmap
    }//Ends the readNewFile method
    
    /**
     *The purpose of this method is to read the information from a specific file
     * back into the clients hashmap of family members
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void readOldFile() throws FileNotFoundException, IOException, ClassNotFoundException{
         //Creates a filepath that is equal to the clients name with .txt added at the end
        String filepath = name+".txt";
        
        //This fileoutputstream object searches for a file with the filename we just
        //created and sees if it already exists. 
        FileInputStream fi = new FileInputStream(new File("src/clientInfo/"+ filepath));
        
        //This objectinputstream object will read any object info we want 
        //from fi
        ObjectInputStream oi = new ObjectInputStream(fi);
        //This reads the information from the file and stores that information
        //in the clients memberList hashmap
        memberList = (HashMap) oi.readObject();

        oi.close();//Closes both stream objects
        fi.close();

    }//Ends the readOldFile method
    
    /**
     *The purpose of this method is to store the hashmap of a client, after having
     * loaded it into the program, in a specific txt file in the project that
     * is named after the client we are saving
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeToFile() throws FileNotFoundException, IOException{
        //Creates a filepath that is equal to the clients name with .txt added at
        //the end
        String filepath = name+".txt";
        //This fileoutputstream object searches for a file with the filename we just
        //created and sees if it already exists. If it doesn't exist then it will 
        //create a new file with that name
        FileOutputStream f = new FileOutputStream(new File("src/clientInfo/"+ filepath));
        
        //This objectoutputstream object will write any object info we want 
        //to the fileoutputstream object/file we just created
        ObjectOutputStream o = new ObjectOutputStream(f);
        
        //This writes out the client's memberlist hashmap to the file
        o.writeObject(memberList);

        o.close();//Closes both objects
        f.close();
    }//Ends the writeToFile method
    
    /**
     *This method takes in a new file and attempts to merge one hashmap with another
     * @param filepath
     * @throws IOException
     */
    public void merge(File filepath) throws IOException{
        
        String line;
        
        BufferedReader reader = new BufferedReader(new FileReader(filepath));
        //The above line created  a BufferedReader object that will use to read the information
        //of the file we passed into the method
        
        while ((line = reader.readLine()) != null) {//This is a while loop that doesn't break
            //until the file we passed in has no more information left to parse
            
            if (!(line.trim().isEmpty())) {//This if statement checks if the current
                //line being looked at is not empty
                
                if (line.contains("parentof")) {//This if statement checks if the 
                    //line contains "parentsof" which indicates there is a relationship
                    //between two familymembers
                    
                    String[] parts = line.split(",");//Splits the line being looked 
                    //at into two parts based on where there is a comma and stores
                    //those splits into a string array
                    
                    //Creates two temp familymember objects that store the 
                    //information of the familymember from the memberList hashmap that has 
                    //their same name
                    FamilyMember parent = memberList.get(parts[0].trim());
                    FamilyMember child = memberList.get(parts[2].trim());
                    
                    //Establishes the familial relationships/bonds between the two 
                    //familyMembers we just grabbed
                    child.addParent(parent);
                    parent.addChild(child);
                } else if (line.contains("marriedto")) {//This if statement checks if the 
                    //line contains "marriedto" which indicates there is a relationship
                    //between two familymembers
                    
                    String[] parts = line.split(",");//Splits the line being looked 
                    //at into two parts based on where there is a comma and stores
                    //those splits into a string array
                    
                    //Creates two temp familymember objects that store the 
                    //information of the familymember from the memberList hashmap that has 
                    //their same name
                    FamilyMember spouse1 = memberList.get(parts[0].trim());
                    FamilyMember spouse2 = memberList.get(parts[2].trim());
                    
                    //Establishes the familial relationships/bonds between the two 
                    //familyMembers we just created
                    spouse1.addSpouse(spouse2);
                    spouse2.addSpouse(spouse1);

                } else {
                    String[] parts = line.split(",", 3);//This if statement checks if the line being looked at doesn't
                    //store any specific key phrases we were previously looking for
                    //in other if statements, which indicates the file is
                    // declaring the actual family member
                    
                    //Creates a familymember object using the splits we just made in
                    //the specific line
                    FamilyMember newMember = new FamilyMember(parts[0].trim(), parts[1].trim(), parts[2].trim());
                    
                    //Adds the familyMember and their name to the membersList
                    memberList.put(parts[0].trim(), newMember);
                }//Ends if statement
            }//Ends if statement

        }//Ends while loop
   
        reader.close();//Closes bufferedreader object
        
    }//Ends merge method
    
   
}//Ends client class


